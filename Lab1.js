let rowToEdit;
let rowToDelete;
let idOfCurrentUser = 0;
const btnAddStudent = document.querySelector("#addButton");
const studentWindow = document.querySelector(".student-window");
const btnAddStudentForm = document.querySelector(".btn--form-submit");
const form = document.querySelector(".student-form");
const studentDeleteWindow = document.querySelector(".delete-student-window");
const btnCloseStudentWindow1 = studentWindow.querySelector(".btn--close-student-window");
const btnCloseStudentWindow2 = studentWindow.querySelector(".btn--form-cancel");
const btnDeleteStudentSubmit = studentDeleteWindow.querySelector(".btn--delete-student-submit");
const btnDeleteStudentCancel1 = studentDeleteWindow.querySelector(".btn--delete-student-cancel");
const btnDeleteStudentCancel2 = studentDeleteWindow.querySelector(".btn--close-student-window");
document.querySelector("#birthday").max = new Date().toISOString().split("T")[0];
if ("serviceWorker" in navigator) {
    self.addEventListener("load", async () => {
        const container = navigator.serviceWorker;
        if (container.controller === null) {
            const reg = await container.register("sw.js");
        }
    });
}
function openAddStudentWindow() {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Add student";
    form.querySelectorAll("input").forEach((input) => (input.value = ""));
    form.querySelectorAll("select").forEach((select) => (select.value = ""));
}
btnAddStudent.addEventListener("click", openAddStudentWindow);

function addStudent(e) {
    let isFormValid = form.checkValidity();
    if (!isFormValid) {
        form.reportValidity();
    } else {
        e.preventDefault();
        form.checkValidity();
        let group, name, gender, birthday;
        group = form.querySelector('[name="group"]').value;
        name = form.querySelector('[name="first-name"]').value + " " +
            form.querySelector('[name="last-name"]').value;
        gender = form.querySelector('[name="gender"]').value;
        birthday = form.querySelector('[name="birthday"]').value;
        if (studentWindow.querySelector("h3").textContent === "Add student") {
            let xhr = new XMLHttpRequest();
            let url = "/adduser?id=" + idOfCurrentUser + "&group=" + group + "&name=" + name + "&gender=" + gender + "&birthday=" + birthday;
            xhr.open("GET", url);
            xhr.send();
            let table = document.getElementById("table");
            let row = table.insertRow(table.length);
            row.id = idOfCurrentUser;
            let cell1 = row.insertCell(0);
            cell1.classList.add("col");
            let cell2 = row.insertCell(1);
            cell2.classList.add("col");
            let cell3 = row.insertCell(2);
            cell3.classList.add("col");
            let cell4 = row.insertCell(3);
            cell4.classList.add("col");
            let cell5 = row.insertCell(4);
            cell5.classList.add("col");
            let cell6 = row.insertCell(5);
            cell6.classList.add("col");
            let cell7 = row.insertCell(6);
            cell7.classList.add("col");
            let span = document.createElement('span');
            let checkbox = document.createElement('input');
            span.appendChild(checkbox);
            checkbox.type = 'checkbox';
            checkbox.addEventListener("click", () => {
                changeCircleColor(cell6)
            });
            cell1.appendChild(span);
            cell2.innerHTML = "<span>" + group + "</span>"
            cell3.innerHTML = "<span>" + name + "</span>"
            cell4.innerHTML = "<span>" + gender + "</span>"
            cell5.innerHTML = "<span>" + birthday + "</span>"
            cell6.innerHTML = '<span class="circle d-inline-block"></span>';
            let spanContainer = document.createElement("span");
            spanContainer.classList.add("buttonContainer", "d-flex", "justify-content-center");
            cell7.appendChild(spanContainer);
            let buttonEdit = document.createElement("button");
            buttonEdit.classList.add("tableButton", "p-0");
            spanContainer.appendChild(buttonEdit);
            let penIcon = document.createElement("i");
            penIcon.classList.add("fa-solid", "fa-pen");
            buttonEdit.appendChild(penIcon);
            let removeButton = document.createElement("button");
            removeButton.classList.add("tableButton", "p-0");
            spanContainer.appendChild(removeButton);
            let removeIcon = document.createElement("i");
            removeIcon.classList.add("fa-solid", "fa-xmark");
            removeButton.appendChild(removeIcon);
           removeButton.addEventListener("click", () => {openDeleteStudentWindow(row)});
           buttonEdit.addEventListener("click", () => {openEditStudentWindow(row)});
           idOfCurrentUser++;
        }else{
            const tds = rowToEdit.querySelectorAll("td");
            tds[1].innerHTML = group;
            tds[2].innerHTML = name;
            tds[3].innerHTML = gender;
            tds[4].innerHTML = birthday;
            let xhr = new XMLHttpRequest();
            let url = "/edituser?" + "id=" + rowToEdit.id +  "&group=" + group + "&name=" + name + "&gender=" + gender + "&birthday=" + birthday;
            xhr.open("GET", url);
            xhr.send();
        }
    }
    studentWindow.classList.remove("opened");
}
btnAddStudentForm.addEventListener("click", addStudent);
function closeStudentWindow() {
    studentWindow.classList.remove("opened");
}

btnCloseStudentWindow1.addEventListener("click", closeStudentWindow);
btnCloseStudentWindow2.addEventListener("click", closeStudentWindow);

function openEditStudentWindow(row) {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Edit student";

    rowToEdit = row;

    const tds = rowToEdit.querySelectorAll("td");

    console.log(rowToEdit);

    const inputs = form.querySelectorAll("input");

    console.log(inputs);

    inputs[0].value = tds[2].innerText.split(" ")[0];
    inputs[1].value = tds[2].innerText.split(" ")[1];
    inputs[2].value = tds[4].innerText;

    const selects = form.querySelectorAll("select");

    selects[0].value = tds[1].innerText;
    selects[1].value = tds[3].innerText;
}
function openDeleteStudentWindow(row) {
    studentDeleteWindow.classList.add("opened");
    rowToDelete = row;
    const name = studentDeleteWindow.querySelector(".student-to-delete");
    const tds = rowToDelete.querySelectorAll("td");
    name.innerText = tds[2].innerText;
}
function closeDeleteWindow(e) {
    studentDeleteWindow.classList.remove("opened");
}
function deleteStudentSubmit(e) {
    e.preventDefault();
    rowToDelete.remove();
    studentDeleteWindow.classList.remove("opened");
    let xhr = new XMLHttpRequest();
    let url = "/deleteuser?" + "id=" + rowToDelete.id;
    xhr.open("GET", url);
    xhr.send();
}
btnDeleteStudentSubmit.addEventListener("click", deleteStudentSubmit);
btnDeleteStudentCancel1.addEventListener("click", closeDeleteWindow);
btnDeleteStudentCancel2.addEventListener("click", closeDeleteWindow);

function removeStudent(el) {
    if(confirm("Are you sure you want to delete user?"))
        el.remove();
}
function changeCircleColor(tableCell){
    let spanCircle = tableCell.querySelectorAll('.circle').item(0);
    let xhr = new XMLHttpRequest();
    let url = "/changestatus?" + "id=" + spanCircle.parentNode.parentNode.id + "&status=";
    if(spanCircle.classList.contains('active')){
        spanCircle.classList.remove('active')
        url += "0";
    }else{
        spanCircle.classList.add('active')
        url += "1";
    }
    xhr.open("GET", url);
    xhr.send();
}